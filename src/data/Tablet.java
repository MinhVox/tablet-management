/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author Quang Minh
 */
public class Tablet {
    private String id;
    private String model;
    private double screenSize;
    private int price;

    public Tablet(String id, String model, double screenSize, int price) {
        this.id = id;
        this.model = model;
        this.screenSize = screenSize;
        this.price = price;
    }

    public String getId() {
        return id;
    }

//    public void setId(String id) {
//        this.id = id;
//    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(double screenSize) {
        this.screenSize = screenSize;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Tablet{" + "id=" + id + ", model=" + model + ", screenSize=" + screenSize + ", price=" + price + '}';
    }
    
    
}
