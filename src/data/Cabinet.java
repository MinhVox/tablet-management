/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.*;
import util.MyToys;

/**
 *
 * @author Quang Minh
 */
public class Cabinet {

    private ArrayList<Tablet> list = new ArrayList();
    private Scanner sc = new Scanner(System.in);

    public void addNewTablet() {
        String id, model;
        double size;
        int price, pos;

        do {
            System.out.print("Input ID: ");
            id = sc.nextLine().trim().toUpperCase();
            pos = searchTbByPos(id);
            if (pos != -1) {
                System.out.println("Input again !!!");
            }
        } while (pos != -1);

        System.out.print("Input Model: ");
        model = sc.nextLine().trim();

        System.out.print("Input Screen size [10;25]: ");
        size = MyToys.getAnDouble(10, 25);

        System.out.print("Input Price [10;100]: ");
        price = MyToys.getAnInteger(10, 100);

        list.add(new Tablet(id, model, size, price));
        System.out.println("Add Sucessfull !!!");
    }

    public int searchTbByPos(String id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equalsIgnoreCase(id)) {
                return i;
            }
        }
        return -1;
    }

    public Tablet searchTbOject(String id) {
        if (list.isEmpty()) {
            return null;
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equalsIgnoreCase(id)) {
                return list.get(i);
            }
        }
        return null;
    }

    public void searchTbByID() {
        String id;
        System.out.print("Input ID: ");
        id = sc.nextLine().trim();
        Tablet x;
        x = searchTbOject(id);
        if (x == null) {
            System.out.println("No ID found");
        } else {
            System.out.println(x);
        }
    }

    public void printListAscByID() {
        System.out.println("List before: ");
        for (Tablet x : list) {
            System.out.println(x);
        }

        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).getId().compareToIgnoreCase(list.get(j).getId()) > 0) {
                    Collections.swap(list, i, j);
                }
            }
        }

        System.out.println("List After: ");
        for (Tablet x : list) {
            System.out.println(x);
        }
    }

    public void updateInfor() {
        String id, tmpModel;
        double tmpSize;
        int tmpPrice;
        System.out.print("Input ID: ");
        id = sc.nextLine().trim();
        Tablet x;
        x = searchTbOject(id);
        if (x == null) {
            System.out.println("No ID found");
        } else {
            System.out.println("ID found:");
            System.out.println(x);

            System.out.print("Input Model: ");
            tmpModel = sc.nextLine().trim();
            x.setModel(tmpModel);

            System.out.print("Input Screen size [10;25]: ");
            tmpSize =  MyToys.getAnDouble(10, 25);
            x.setScreenSize(tmpSize);

            System.out.print("Input Price [10;100]: ");
            tmpPrice = MyToys.getAnInteger(10, 100);
            x.setPrice(tmpPrice);
            
            System.out.println("Update sucessfull");
        }
    }
    
    public void removeTbByID(){
        String id;
        System.out.print("Input ID: ");
        id = sc.nextLine().trim();
        Tablet x;
        x = searchTbOject(id);
        if (x == null) {
            System.out.println("No ID found");
        } else {
            list.remove(x);
            System.out.println("Remove sucessfull!!");
        }
    }
}
