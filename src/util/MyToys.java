/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Scanner;

/**
 *
 * @author Quang Minh
 */
public class MyToys {

    private static Scanner sc = new Scanner(System.in);

    public static int getAnInteger(int min, int max) {
        int n;
        while (true) {
            try {
                do {
                    n = Integer.parseInt(sc.nextLine());
                    if (n < min || n > max) {
                        System.out.println("Input again !!!");
                    }
                } while (n < min || n > max);
                return n;
            } catch (Exception e) {
                System.out.println("Input again !!!");
            }
        }
    }
    
    public static double getAnDouble(double min, double max) {
        double n;
        while (true) {
            try {
                do {
                    n = Double.parseDouble(sc.nextLine());
                    if (n < min || n > max) {
                        System.out.println("Input again !!!");
                    }
                } while (n < min || n > max);
                return n;
            } catch (Exception e) {
                System.out.println("Input again !!!");
            }
        }
    }
}
