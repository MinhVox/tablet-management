/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe_se130010;

import data.Cabinet;
import ui.Menu;

/**
 *
 * @author Quang Minh
 */
public class PE_SE130010 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Menu option = new Menu("New Age Store");

        option.getOption("1.Add a tablet to the list");
        option.getOption("2.Search a tablet by id");
        option.getOption("3.Update a tablet by id");
        option.getOption("4.Remove a tablet by id");
        option.getOption("5.Print the list of tablet in the asceding order of id");
        option.getOption("6.Quit");

        option.printMenu();
        int choice;
        Cabinet box = new Cabinet();
        do {
            System.out.println("Input a number :");
            choice = option.getChoice();
            switch (choice) {
                case 1:
                    System.out.println("You choose #1");
                    box.addNewTablet();
                    System.out.println();
                    break;
                case 2:
                    System.out.println("You choose #2");
                    box.searchTbByID();
                    System.out.println();
                    break;
                case 3:
                    System.out.println("You choose #3");
                    box.updateInfor();
                    System.out.println();
                    break;
                case 4:
                    System.out.println("You choose #4");
                    box.removeTbByID();
                    System.out.println();
                    break;
                case 5:
                    System.out.println("You choose #5");
                    box.printListAscByID();
                    System.out.println();
                    break;
                case 6: 
                    System.out.println("GoodBye !!!");
                    break;
            }
        } while (choice != 6);
    }

}
