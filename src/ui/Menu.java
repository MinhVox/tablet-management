/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.util.*;
import util.MyToys;

/**
 *
 * @author Quang Minh
 */
public class Menu {
    private String appName;
    private ArrayList<String> option = new ArrayList();

    public Menu(String appName) {
        this.appName = appName;
    }
    
    public void getOption(String option){
        this.option.add(option);
    }
    
    public void printMenu(){
        System.out.println("Welcome to " + appName +" @2018 by <SE130010 - Võ Quang Minh>");
        for (String x : option) {
            System.out.println(x);
        }
    }
    
    public int getChoice(){
        return MyToys.getAnInteger(1,6);
    }
}
